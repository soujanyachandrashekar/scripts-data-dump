import sys
import pandas as pd
import json
import csv

filename = sys.argv[1]
print(filename)
pd.options.display.max_colwidth = 1000
chunksize = 1
sessions = []
f = open("data.txt", "w+")
for chunk in pd.read_csv(filename, chunksize=chunksize, dtype="str"):
    sessions.append(chunk['sessionId'].tolist()[0])

sessions = list(set(sessions))

for i in sessions:
    f.write(i + '\n')

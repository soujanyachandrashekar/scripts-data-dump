var argv = require('minimist')(process.argv.slice(2));
const filename = (argv["_"][0].split("/").slice(-1)[0].split(".")[0]);
fullfile = argv["_"][0].split("/").slice(-1)[0];
const fs = require('fs')
const { Parser } = require('json2csv');
// const fields = ["_id","recipient","status","timestamp"]
const fields_error = ["error_code", "error_href", "error_title", "id","recipient_id", "status", "timestamp"];
const parser = new Parser({ header:false});
const parser_error = new Parser({fields_error, header:false});
var logger_real = fs.createWriteStream(filename+'_data.csv', {
  flags: 'a'
})
var logger_errors = fs.createWriteStream(filename+'_errors.csv', {
  flags: 'a'
})

logger_real.write("\"_id\",\"recipient\",\"status\",\"timestamp\"\n");
logger_errors.write("\"error_code\", \"error_href\", \"error_title\", \"id\",\"recipient_id\", \"status\", \"timestamp\"\n")
var lineReader = require('readline').createInterface({
  input: require('fs').createReadStream(fullfile)
});

lineReader.on('line', function (line) {
  line = line.replace(/""/g,'"');
  line = line.substring(1, line.length - 1)
  parsed_json = JSON.parse(line);
  // console.log(parsed_json);
  if(parsed_json.errors){
    new_obj = {};
    new_obj.error_code = parsed_json.errors[0].code;
    new_obj.error_href = parsed_json.errors[0].href;
    new_obj.error_title = parsed_json.errors[0].title;
    new_obj.id = parsed_json.id;
    new_obj.recipient_id = parsed_json.recipient_id;
    new_obj.status = parsed_json.status;
    new_obj.timestamp = parsed_json.timestamp;

    csv = parser_error.parse(new_obj);
    logger_errors.write(csv+'\n');
    console.log(csv);
  }
  else {
    csv = parser.parse(parsed_json);
    logger_real.write(csv + '\n');
  }
});


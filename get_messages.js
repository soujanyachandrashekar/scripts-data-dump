const mongoose = require('mongoose');
const mkdirp = require('mkdirp');
const Promise = require('bluebird');
const { readFileSync} = require('fs');
const fs = require('fs');
const { parse } = require('json2csv');

mongoose.Promise = Promise;
const configFile = readFileSync('./config.json').toString();
config = typeof configFile === 'string' ? JSON.parse(configFile) : configFile;

const dbName = 'vault';
const collectionName = 'messages';
const profileSchema = mongoose.Schema({
    uid: String,
    botId: String,
    message: String,
    messageType: String,
    sessionId: String,
    slug: String,
    feedback: String,
    source: String,
    __v: Number
});
const fields = ['_id', 'created', 'updated', 'botId', 'uid', 'message', 'messageType', 'sessionId', 'slug', 'feedback', 'source', '__v'];
const opts = { fields };

const testBotId = (botId) => {
    if (!botId.startsWith('x')) {
        return false;
    }
    return true;
}

let botId = process.argv[2];

if (!botId || !testBotId(botId)) {
    console.log('Please enter botId as command line argument.');
    process.exit();
}

// const vault = mongoose.createConnection(config.mongo.replSetUrl + '/vault' + config.mongo.replSetOption, {
const vault = mongoose.createConnection('mongodb://studio.botplatform.io:27017/'+ dbName, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let collection = vault.model(collectionName, profileSchema);

const getVaultDataByBotId = (collection, botId) => {
    return collection.find({botId}).lean();
}

const saveMessageDataToCsv = (botId) => {
    getVaultDataByBotId(collection, botId).then((data) => {
        if (data.length) {
            // Save JSON and CSV files in a folder named by it's botId
            let basepath = `./${botId}/`;
            mkdirp(basepath);

            let pathJSON = `./${botId}/${botId}_${collectionName}_${new Date().toISOString()}.json`;
            fs.writeFileSync(pathJSON, JSON.stringify(data));

            let pathCSV = `./${botId}/${botId}_${collectionName}_${new Date().toISOString()}.csv`;
            let csvData = parse(data, opts);
            fs.writeFileSync(pathCSV, csvData);
            console.log('Data stored successfully in the folder named by bot Id !! ')
        } else {
            console.log('No data found!')
        }
    })
}

saveMessageDataToCsv(botId)

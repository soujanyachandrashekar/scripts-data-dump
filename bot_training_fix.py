# !/usr/bin/env python
# -*- coding: utf-8 -*-
from pymongo import MongoClient
import json
import requests
import datetime
from elasticsearch import Elasticsearch
es = Elasticsearch(["es-node-0:9200","es-node-1:9200","es-node-2:9200"])
client = MongoClient('mongodb://mongodb-0:27017,mongodb-1:27017,mongodb-2:27017/?replicaSet=rs0')
db = client['models']
old_bot='x1546435694619'
all_trainings = list(db.training.find({"bot_id": old_bot}))
intents=[]
for training in all_trainings:
    if "text" in training.keys() and "intent" in training.keys():
        if training["intent"] not in intents:
            intents.append(training["intent"])
db_intents = db.intents.find_one({"bot_id": old_bot})
db_intents["intents"] = intents
db.intents.replace_one({"bot_id": old_bot}, db_intents, True)
print "done"

const enc = require('./crypt_lib.js');
const fs = require('fs')
const { Parser } = require('json2csv');
const fields = ["_id","created","updated","botId","uid","message","messageType","sessionId","slug","feedback","source","__v","data"]
const parser = new Parser({fields, header:false});
var logger = fs.createWriteStream('parsed_data_output_messages_.csv', {
  flags: 'a'
})
var lineReader = require('readline').createInterface({
  input: require('fs').createReadStream('x1545404974164_messages.json')
});

logger.write("_id,created,updated,botId,uid,message,messageType,sessionId,slug,feedback,source,__v,data\n");
lineReader.on('line', function (line) {
    parsed_json = JSON.parse(line);
    console.log(parsed_json)
    parsed_json._id = (parsed_json._id.$oid)
    if ( parsed_json.created && parsed_json.updated ) {
    parsed_json.created = (parsed_json.created['$date']['$numberLong'])
    parsed_json.updated = (parsed_json.updated['$date']['$numberLong'])
    }
    parsed_json.__v = parsed_json.__v.$numberInt
    parsed_json.message = enc.decrypt(parsed_json.message);
    csv = parser.parse(parsed_json);
    //    console.log(parsed_json);
    // console.log(csv);

    logger.write(csv+'\n');
});
lineReader.on('close', function () {
    logger.end();
});
//console.log(enc.decrypt(name));



from pymongo import MongoClient
from bson.objectid import ObjectId
import config
configuration = {}
configuration = config.ProductionConfig()
mongo_url = configuration.ML_MONGO_URL
client = MongoClient(mongo_url)
db = client['models']
#this function should be run when  training doesnt work for some bots while restarting celery
def set_training_false():
    data=list(db.intents.find({"is_training":True}))
    for bot in data:
        try:
            bot["is_training"]=False
            db.intents.replace_one({"bot_id": bot["bot_id"]}, bot, True)
        except:
            continue
    print data
if __name__ == "__main__":
    set_training_false()

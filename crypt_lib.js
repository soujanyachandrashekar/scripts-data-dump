const crypto = require('crypto');
module.exports = {
    encrypt: (text,options) => {
        let algorithm = 'aes-256-ctr';
        let key = 'tL9M3ah5MdUo6wkg1IKqSZRquoPl1V3N';
        let iv = 'initializationIV';
        let encoding = 'hex';
        if(options && options.algorithm) {
            algorithm = options.algorithm;
        }
        if(options && options.key) {
            key = options.key;
        }
        if(options && options.iv) {
            iv = options.iv;
        }
        if(options && options.encoding) {
            encoding = options.encoding;
        }
        let cipher;
        if(options && options.useIv) {
            cipher = crypto.createCipheriv(algorithm, key, options.iv || iv);
        } else {
            cipher = crypto.createCipher(algorithm, key);
        }


        let crypted = cipher.update(text,'utf8',encoding);
        crypted += cipher.final(encoding);
        return crypted;
    },
    decrypt: (text,options) => {
        let algorithm = 'aes-256-ctr';
        let key = 'tL9M3ah5MdUo6wkg1IKqSZRquoPl1V3N';
        let iv = 'initializationIV';
        let encoding = 'hex';
        if(options && options.algorithm) {
            algorithm = options.algorithm;
        }
        if(options && options.key) {
            key = options.key;
        }
        if(options && options.iv) {
            iv = options.iv;
        }
        if(options && options.encoding) {
            encoding = options.encoding;
        }
        let decipher;
        if(options && options.useIv) {
            decipher = crypto.createDecipheriv(algorithm, key,options.iv || iv);
        } else {
            decipher = crypto.createDecipher(algorithm, key);
        }
        let decrypted = decipher.update(text,encoding,'utf8');
        decrypted += decipher.final();
        return decrypted;
    }
};

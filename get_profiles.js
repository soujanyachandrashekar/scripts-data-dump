// const config = require('./config.js');
const mongoose = require('mongoose');
const mkdirp = require('mkdirp');
const Promise = require('bluebird');
const { readFileSync } = require('fs');
const fs = require('fs');
const _ =require('lodash');
const { parse } = require('json2csv');
mongoose.Promise = Promise;
const configFile = readFileSync('./config.json').toString();
config = typeof configFile === 'string' ? JSON.parse(configFile) : configFile;
// config = config.config;
const vault = mongoose.createConnection(config.mongo.replSetUrl + '/vault' + config.mongo.replSetOption, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
const profileSchema = mongoose.Schema({
    uid: String,
    bot: String,
    platform: String,
    data: mongoose.Schema.Types.Mixed,
    firstTime: Boolean,
    __v: Number
});

const testBotId = (botId) => {
    if (!botId.startsWith('x')) return false;
    try {
        a = new Date(Number(botId.split('x')[1]));
    } catch (e) {
        return false;
    }
    return true;
};

let botId = process.argv[2];

if (!botId || !testBotId(botId)) {
    console.log('please enter botid as a command line arg');
    process.exit();
}

let collectionNameList = ['cold_profiles', 'hot_profiles', 'cold_profiles_backups', 'profiles_dec_2018', 'profiles_december_2018', 'profiles_february_2018', 'profiles_january_2018', 'profiles_march_2018', 'profiles_oct_2018', 'profiles_october_2018'];
let collectionList = [];
for (let i = 0; i < collectionNameList.length; i++) {
    collectionList.push(vault.model(collectionNameList[i], profileSchema));
}

const getVaultDataByBotId = async (collection, botId) => {
    return collection.find({ bot: botId }).lean();
};
// https://stackoverflow.com/questions/19098797/fastest-way-to-flatten-un-flatten-nested-json-objects
Object.flatten = function(data) {
    var result = {};
    function recurse (cur, prop) {
        if (Object(cur) !== cur) {
            result[prop] = cur;
        } else if (Array.isArray(cur)) {
            for(var i=0, l=cur.length; i<l; i++)
                recurse(cur[i], prop + "[" + i + "]");
            if (l == 0)
                result[prop] = [];
        } else {
            var isEmpty = true;
            for (var p in cur) {
                isEmpty = false;
                recurse(cur[p], prop ? prop+"."+p : p);
            }
            if (isEmpty && prop)
                result[prop] = {};
        }
    }
    recurse(data, "");
    return result;
}

const saveData = async (botId) => {
    for (let i = 0; i < collectionList.length; i++) {
        let vaultData = await getVaultDataByBotId(collectionList[i], botId);
        console.log(vaultData);
        // make this in a folder named botID and put date along with each file
        let basepath = `./${botId}/`;
        await mkdirp(basepath);
        //JSON
        let pathJSON = `./${botId}/${botId}_${collectionNameList[i]}_${new Date().toISOString()}.json`;
        fs.writeFileSync(pathJSON, JSON.stringify(vaultData));
        //CSV
        let pathCSV = `./${botId}/${botId}_${collectionNameList[i]}_${new Date().toISOString()}.csv`;
        vaultData = _.map(vaultData, (x) => {
            y = Object.flatten(x.data);
            delete y.__proto__;
            delete x.data;
            x = _.assign(x, y);
            return x;
        })
        if (Array.isArray(vaultData) && vaultData.length) {
            let csvData = parse(vaultData, {});
            console.log(csvData);
            fs.writeFileSync(pathCSV, csvData);
        } else {
            console.log('vault data for ' + botId + ' ' + collectionNameList[i] + ' is empty');
        }
    }
};

try {
    saveData(botId).then(() => {
        console.log("done");
    })
} catch (e) {
    console.log(e);
}
